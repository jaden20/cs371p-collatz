// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iterator> // istream_iterator
#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// ----
// read
// ----

TEST(CollatzFixture, read) {
    ASSERT_EQ(collatz_read("1 10\n"), make_pair(1, 10));
}

// ----
// eval
// ----

TEST(CollatzFixture, eval0) {
    ASSERT_EQ(collatz_eval(make_pair(1, 10)), make_tuple(1, 10, 20));
}

TEST(CollatzFixture, eval1) {
    ASSERT_EQ(collatz_eval(make_pair(100, 200)), make_tuple(100, 200, 125));
}

TEST(CollatzFixture, eval2) {
    ASSERT_EQ(collatz_eval(make_pair(201, 210)), make_tuple(201, 210, 89));
}

TEST(CollatzFixture, eval3) {
    ASSERT_EQ(collatz_eval(make_pair(900, 1000)), make_tuple(900, 1000, 174));
}

TEST(CollatzFixture, eval4) {
    ASSERT_EQ(collatz_eval(make_pair(1, 1)), make_tuple(1, 1, 1));
}

TEST(CollatzFixture, eval5) {
    ASSERT_EQ(collatz_eval(make_pair(4, 5)), make_tuple(4, 5, 6));
}

// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream oss;
    collatz_print(oss, make_tuple(1, 10, 20));
    ASSERT_EQ(oss.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream iss("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream oss;
    collatz_solve(iss, oss);
    ASSERT_EQ(oss.str(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n");
}

// ------------
// cycle_length
// ------------

TEST(CollatzFixture, cycle_length1) {
    build_cache();
    ASSERT_EQ(cycle_length(997823), 440);
}

TEST(CollatzFixture, cycle_length2) {
    build_cache();
    ASSERT_EQ(cycle_length(680303), 248);
}

TEST(CollatzFixture, cycle_length3) {
    build_cache();
    ASSERT_EQ(cycle_length(733945), 88);
}

TEST(CollatzFixture, cycle_length4) {
    build_cache();
    ASSERT_EQ(cycle_length(56006), 35);
}

TEST(CollatzFixture, cycle_length5) {
    build_cache();
    ASSERT_EQ(cycle_length(154462), 171);
}

TEST(CollatzFixture, cycle_length6) {
    build_cache();
    ASSERT_EQ(cycle_length(33886), 60);
}

TEST(CollatzFixture, cycle_length7) {
    build_cache();
    ASSERT_EQ(cycle_length(297120), 40);
}

TEST(CollatzFixture, cycle_length8) {
    build_cache();
    ASSERT_EQ(cycle_length(742996), 150);
}

TEST(CollatzFixture, cycle_length9) {
    build_cache();
    ASSERT_EQ(cycle_length(940590), 153);
}

TEST(CollatzFixture, cycle_length10) {
    build_cache();
    ASSERT_EQ(cycle_length(384339), 211);
}

TEST(CollatzFixture, cycle_length11) {
    build_cache();
    ASSERT_EQ(cycle_length(74311), 113);
}

TEST(CollatzFixture, cycle_length12) {
    build_cache();
    ASSERT_EQ(cycle_length(490604), 258);
}

TEST(CollatzFixture, cycle_length13) {
    build_cache();
    ASSERT_EQ(cycle_length(12764), 77);
}


TEST(CollatzFixture, cycle_length14) {
    build_cache();
    ASSERT_EQ(cycle_length(661898), 155);
}

TEST(CollatzFixture, cycle_length15) {
    build_cache();
    ASSERT_EQ(cycle_length(70619), 175);
}

TEST(CollatzFixture, cycle_length16) {
    build_cache();
    ASSERT_EQ(cycle_length(37565), 63);
}

TEST(CollatzFixture, cycle_length17) {
    build_cache();
    ASSERT_EQ(cycle_length(6047), 94);
}

TEST(CollatzFixture, cycle_length18) {
    build_cache();
    ASSERT_EQ(cycle_length(371830), 180);
}

TEST(CollatzFixture, cycle_length19) {
    build_cache();
    ASSERT_EQ(cycle_length(92541), 134);
}




