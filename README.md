# CS371p: Object-Oriented Programming Collatz Repo

* Name: Jaden Hyde

* EID: jah8624

* GitLab ID: jaden20

* HackerRank ID: jadenhyde201

* Git SHA: e4a0fa25443c385eff83421c9e85f91a73ac464c

* GitLab Pipelines: https://gitlab.com/jaden20/cs371p-collatz/-/pipelines

* Estimated completion time: 8

* Actual completion time: 15

* Comments: N/A
